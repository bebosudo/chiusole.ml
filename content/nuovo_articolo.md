Title: Qui va inserito il titolo
Date: 2023-02-01 22:30
Category: Lotti
Tags: Apicoltura
Ref_Image: Qua-inserire-il-nome-di-un-immagine-da-usare-come-copertina.jpg

Tutto quello che viene dopo la prima riga bianca viene considerato come corpo dell'articolo.

Il corpo va scritto usando la formattazione Markdown: al seguente link e' disponibile una guida in italiano (https://www.html.it/articoli/markdown-guida-al-linguaggio/), ma in inglese si trovano guide piu' complete, come questa veloce introduzione a Markdown in 5 minuti (https://about.gitlab.com/blog/2018/08/17/gitlab-markdown-tutorial/), oppure il manuale di GitLab, la piattaforma che ospita questo sito (https://about.gitlab.com/handbook/markdown-guide/).

Ad esempio, per scrivere in grassetto, basta circondare il testo con **due asterischi**, cosi'.
Per scrivere in corsivo, ne basta *una coppia*.
Per inserire un link si puo' fare [cosi'](https://google.com).

Se si vuole testare il proprio codice markdown ci sono molti editor online per farlo in simultanea mentre si scrive, ad esempio i primi due che ho trovato sono https://markdownlivepreview.com e https://markdown-editor.github.io.
