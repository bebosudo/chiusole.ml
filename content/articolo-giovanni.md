Title: L'acqua nella produzione di miele
Date: 2023-02-06 16:56
Category: Sostenibilità e innovazione
Tags: Apicoltura
Ref_Image: 

è possibile monitorare lo stato di salute della api monitorando la qualità dell'acqua?

Di seguito c'e' un esempio di video youtube "embeddato" con un iframe, vediamo se funziona:

<iframe
    width="640"
    height="480"
    src="https://www.youtube.com/embed/2Gg6Seob5Mg"
    frameborder="0"
    allow="autoplay; encrypted-media"
    allowfullscreen
>
</iframe>

Altrimenti la seguente immagine in realta' e' un link a Youtube, quindi che porta l'utente fuori dal sito attuale:

[![IMAGE_ALT](https://img.youtube.com/vi/2Gg6Seob5Mg/0.jpg)](https://www.youtube.com/watch?v=2Gg6Seob5Mg)
